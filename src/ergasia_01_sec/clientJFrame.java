package ergasia_01_sec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class clientJFrame extends JFrame implements ActionListener {

    //Orismos component
    JPanel row1 = new JPanel();
    JLabel enterIpLabel = new JLabel("Enter IP (or leave localhost)", JLabel.RIGHT);
    JTextField ipTextField_1 = new JTextField(3);
    JLabel ipLabel_1 = new JLabel(".", JLabel.RIGHT);
    JTextField ipTextField_2 = new JTextField(3);
    JLabel ipLabel_2 = new JLabel(".", JLabel.RIGHT);
    JTextField ipTextField_3 = new JTextField(3);
    JLabel ipLabel_3 = new JLabel(".", JLabel.RIGHT);
    JTextField ipTextField_4 = new JTextField(3);
    JLabel ipLabel_4 = new JLabel(".", JLabel.RIGHT);


    JPanel row2 = new JPanel();
    JButton myButton = new JButton("Exit");
    JButton identifyButton = new JButton("Identify");


    JPanel row3 = new JPanel();
    JLabel clientLogLabel = new JLabel("Client's log: ");
    public static JTextArea clientLogTextArea = new JTextArea("Hello, I am the client. \n", 5, 30);
    JScrollPane scroll = new JScrollPane(clientLogTextArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    String log = "Hello, I am the clint";


    //constructor

    public clientJFrame() {
        super("This is the title");

        Container myPane = getContentPane();

        GridLayout myGridLayout = new GridLayout(3, 1, 20, 20);
        myPane.setLayout(myGridLayout);


        //proti grammi
        FlowLayout layoutLine1 = new FlowLayout();
        row1.setLayout(layoutLine1);
        row1.add(enterIpLabel);
        row1.add(ipTextField_1);
        row1.add(ipLabel_1);
        row1.add(ipTextField_2);
        row1.add(ipLabel_2);
        row1.add(ipTextField_3);
        row1.add(ipLabel_3);
        row1.add(ipTextField_4);
        myPane.add(row1);

        //deyteri grammi
        FlowLayout layoutLine2 = new FlowLayout();
        row2.setLayout(layoutLine2);
        row2.add(identifyButton);
        row2.add(myButton);
        myPane.add(row2);

        //triti grammi
        FlowLayout layoutLine3 = new FlowLayout();
        row3.setLayout(layoutLine3);
        row3.add(clientLogLabel);
        row3.setPreferredSize(new Dimension(400, 150));
        row3.add(scroll);
        myPane.add(row3);


        myButton.addActionListener(this);
        identifyButton.addActionListener(this);

        /**
         * with lambda
         * myButton.addActionListener( e -> {        });
         */


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.setLocation(400, 300);
        pack();
    }


//    public static void main(String[] args) {
//        new clientJFrame();
//    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()== myButton) {
            clientLogTextArea.append("Exiting...\n");
            System.exit(0);
        } else  {
            System.out.println("iden");
        }
    }
}
