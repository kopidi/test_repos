package com.test_project_01;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class cli {
    public static void main (String args[]) throws IOException {

//        creating a scanner in order to take input from the user
        Scanner sc = new Scanner(System.in);

//        Create the client socket with the essential parameters (IP address, Port #)
        Socket s = new Socket("127.0.0.1",1342);

//        Scanner to get the input of the server
        Scanner sc1 = new Scanner(s.getInputStream());
        System.out.println("Enter any number:");

//        Accept the number from the user and store the input
        int number = sc.nextInt();

//        pass the number at the server
        PrintStream p = new PrintStream(s.getOutputStream());
        p.println(number);

//        accept the result from the server
        int temp = sc1.nextInt();
        System.out.println("The server sent me the number: " + temp);
    }
}
