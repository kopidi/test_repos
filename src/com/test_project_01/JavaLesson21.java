package com.test_project_01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class JavaLesson21 extends JFrame {

    JButton button1;
    JTextField textField1;
    JTextArea textArea1;
    int buttonClicked;

    public static void main(String[] args) {

        new JavaLesson21();
    }

    public JavaLesson21() {

        //properties of the frame
        this.setSize(500, 500);
        this.setLocation(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Client");
        //this.getContentPane().setBackground(Color.decode("#5FB795"));


        JPanel thePanel = new JPanel();
        JLabel label1 = new JLabel("This is the client.");
        //label1.setText("This is new!");
        label1.setToolTipText("Just some info");
        thePanel.add(label1);
        thePanel.setBackground(Color.decode("#5FB795"));

        button1 = new JButton("Click to connect");
        thePanel.add(button1);
        //creates an instance waiting for events
        ListenForButton lForButton = new ListenForButton();
        button1.addActionListener(lForButton);

        textField1 = new JTextField("Type here", 15);
        thePanel.add(textField1);
        //creates an instance waiting for events
        ListenForKeys lForKeys = new ListenForKeys();
        textField1.addKeyListener(lForKeys);

        /**
         * textArea implementation
         */
        textArea1 = new JTextArea(15, 20);
        textArea1.setLineWrap(true);
        textArea1.setWrapStyleWord(true);   //not to split words
        //scroll bar implementation
        JScrollPane scrollbar1 = new JScrollPane(textArea1, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        thePanel.add(scrollbar1);
        thePanel.add(textArea1);

        /**
         * Windows change implementation
         */
        ListenForWindow lForWindow = new ListenForWindow();
        this.addWindowListener(lForWindow);


        /**
         * prosthiki tou panel kai setVisible
         */
        this.add(thePanel);
        //invoke setVisible AFTER adding components
        this.setVisible(true);
    }

    /**
     *
     */


    /**
     * Implement Listeners
     */
    private class ListenForButton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == button1) {
                buttonClicked++;
                textArea1.append("Button clicked " + buttonClicked + " times\n");
//                e.getSource().toString();
            }
        }
    }

    private class ListenForKeys implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            textArea1.append("Key Hit: " + e.getKeyChar() + "\n");
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    private class ListenForWindow implements WindowListener {
        @Override
        public void windowOpened(WindowEvent e) {

        }

        @Override
        public void windowClosing(WindowEvent e) {

        }

        @Override
        public void windowClosed(WindowEvent e) {

        }

        @Override
        public void windowIconified(WindowEvent e) {

        }

        @Override
        public void windowDeiconified(WindowEvent e) {

        }

        @Override
        public void windowActivated(WindowEvent e) {
            textArea1.append("Window is active.\n");
        }

        @Override
        public void windowDeactivated(WindowEvent e) {

        }
    }
}
