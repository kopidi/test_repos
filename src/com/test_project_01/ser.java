package com.test_project_01;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ser {
    public  static  void main(String args[]) throws IOException {

//        Create the server socket. We specify the same port number that we put an the client class
        ServerSocket s1 = new ServerSocket(1342);
        System.out.println("Server is up and running! ");
        System.out.println("Server is waiting for connections... ");
        System.out.println();

        Socket ss = s1.accept();
        System.out.println("A client connected to the Server");

//        create a scanner object
        Scanner sc = new Scanner(ss.getInputStream());

        int number = sc.nextInt();
        System.out.println("Number " + number + " received..");

        int response = number * 2;

//        pass the response to the client. need a printStream object

        PrintStream p = new PrintStream(ss.getOutputStream());
        p.println(response);
        System.out.println("I have sent the number " + response);
    }

}
