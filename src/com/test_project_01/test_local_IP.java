package com.test_project_01;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class test_local_IP {

        public static void main(String a[]){

            try {
                InetAddress host = InetAddress.getByName("localhost");
                System.out.println(host.getHostAddress());
            } catch (UnknownHostException ex) {
                ex.printStackTrace();
            }
        }
    }

