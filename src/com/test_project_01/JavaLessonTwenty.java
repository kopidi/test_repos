package com.test_project_01;

import javax.swing.*;
import java.awt.*;

public class JavaLessonTwenty extends JFrame {
    public static void main(String[] args) {

        new JavaLessonTwenty();
    }

    public JavaLessonTwenty() {

        //properties of the frame
        this.setSize(500, 500);
        this.setLocation(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Client");
        //this.getContentPane().setBackground(Color.decode("#5FB795"));


        JPanel thePanel = new JPanel();
        JLabel label1 = new JLabel("This is the client");
        //label1.setText("This is new!");
        label1.setToolTipText("Just some info");
        thePanel.add(label1);
        this.add(thePanel);
        thePanel.setBackground(Color.decode("#5FB795"));


        JButton buttonConnect = new JButton("Connect");
        buttonConnect.setToolTipText("Press to connect to the server");
        JButton buttonClose = new JButton("Close");
        buttonClose.setToolTipText("Press to close the connection from the server");
        thePanel.add(buttonConnect);
        thePanel.add(buttonClose);


        JTextField textField1 = new JTextField("Type here", 25);
        textField1.setToolTipText("Something");
        thePanel.add(textField1);


        JTextArea textArea1 = new JTextArea(15, 20);
        textArea1.setToolTipText("Something else");
        textArea1.setText("This is some random text");
        textArea1.setLineWrap(true);
        textArea1.append("\n\nNumber of lines: " + textArea1.getLineCount());
        thePanel.add(textArea1);

        JScrollPane scrollbar1 = new JScrollPane(textArea1, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        thePanel.add(scrollbar1);

        //toolkit allows us to ask questions to the OS
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        System.out.println(tk.getScreenSize());


        //invoke setVisible AFTER adding components
        this.setVisible(true);
        //how to focus. After adding components
        textField1.requestFocus();
    }

}
