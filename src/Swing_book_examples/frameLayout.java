package Swing_book_examples;

import javax.swing.*;
import java.awt.*;

public class frameLayout extends JFrame {

    //Orismos component
    JPanel row1 = new JPanel();

    JLabel myLabel = new JLabel("####", JLabel.LEFT);
    JTextField myTextField = new JTextField(15);



    JPanel row2 = new JPanel();

    JButton myButton = new JButton("#####");



    //constructor
    public frameLayout () {
        super("This is the title");

        Container myPane = getContentPane();


        GridLayout myGridLayout = new GridLayout(2,1);
        myPane.setLayout(myGridLayout);

        //proti grammi
        FlowLayout myFlowLayout1 = new FlowLayout();
        row1.setLayout(myFlowLayout1);
        row1.add(myLabel);
        row1.add(myTextField);
        myPane.add(row1);


        //deyteri grammi
        FlowLayout myFlowLayout2 = new FlowLayout();
        row2.setLayout(myFlowLayout2);
        row2.add(myButton);
        row2.add(myTextField);
        myPane.add(row2);

//        setContentPane(myPane);

        pack();
        this.setLocation(400, 300);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
