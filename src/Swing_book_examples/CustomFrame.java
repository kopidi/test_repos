package Swing_book_examples;

import javax.swing.*;
import java.awt.*;

public class CustomFrame extends JFrame {

    JButton closeButton = new JButton("Close");

    public CustomFrame() {
        super("Simplest Event Handling JFrame");
        this.initFrame();
    }

    private void initFrame() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
// Set a FlowLayout for the content pane
        this.setLayout(new FlowLayout());
// Add the Close JButton to the content pane
        this.getContentPane().add(closeButton);
// Add an ActionListener to closeButton
//        closeButton.addActionListener(e -> System.exit(0));
    }

    public static void main(String[] args) {
        CustomFrame frame = new CustomFrame();
        frame.pack();
        frame.setVisible(true);
    }
}