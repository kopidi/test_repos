package Swing_book_examples;

import javax.swing.*;
import java.awt.*;

public class ex_01 {

    public static void main(String[] args) {

        /**
         * Creates a JFrame object
         */
        JFrame frame = new JFrame("Simplest Swing");

        Container contentPane = frame.getContentPane();


        //button
        JButton closeButton = new JButton("Close");
        JButton helpButton = new JButton("Help");
        contentPane.add(closeButton);
        contentPane.add(helpButton);


        LayoutManager myLayoutManager = new FlowLayout();
        frame.setLayout(myLayoutManager);


        // Make the JFrame visible on the screen
        frame.setBounds(400, 650, 500, 400);

//        for(int i = 1; i <= 04; i++) {
//            contentPane.add(new JButton("Button " + i));
//        }

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        Box myBox =  Box.createHorizontalBox();
    }
}



